import { NestFactory } from "@nestjs/core";
import { ValidationPipe, ValidationPipeOptions, VersioningType } from "@nestjs/common";
import { AppModule } from "./app.module";
import { ConfigService } from "@nestjs/config";
import ConfigEnum from "./configs/config.enum";
import { SharedConfigOptions } from "./configs/shared.config";
import TypeORMExceptionFilter from "@shared/filters/typeorm-exception.filter";
import { LoggerService } from "@shared/logger/logger.service";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule, {
    logger: new LoggerService(),
  });

  const configService = app.get(ConfigService);

  const sharedConfig = configService.get<SharedConfigOptions>(ConfigEnum.SHARED);
  const validationConfig = configService.get<ValidationPipeOptions>(ConfigEnum.VALIDATION);

  app.setGlobalPrefix("api");
  app.enableCors();
  app.enableVersioning({
    type: VersioningType.URI,
  });
  app.useGlobalPipes(new ValidationPipe(validationConfig));
  app.useGlobalFilters(new TypeORMExceptionFilter());

  const swaggerOptions = new DocumentBuilder()
    .setTitle(sharedConfig.name)
    .setDescription("API Documentation for profio proxy service")
    .setVersion(sharedConfig.version)
    .addBearerAuth({
      type: "http",
      scheme: "bearer",
      bearerFormat: "JWT",
      name: "JWT",
      description: "Enter JWT token",
      in: "header",
    })
    .build();

  const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions);

  SwaggerModule.setup("/api/docs", app, swaggerDoc);

  await app.listen(sharedConfig.port);
};

void bootstrap();
