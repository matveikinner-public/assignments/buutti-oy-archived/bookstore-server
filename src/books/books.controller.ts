import { Body, Controller, Delete, Get, Param, Post, Put, Query, Version } from "@nestjs/common";
import PagedRequestDto from "@shared/dtos/paged-request.dto";
import UuidDto from "@shared/dtos/uuid.dto";
import { BooksService } from "./books.service";
import BookDto from "./dtos/create-book.dto";
import UpdateBookDto from "./dtos/update-book.dto";

@Controller("books")
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  @Version("1")
  @Get()
  getBooks(@Query() query: PagedRequestDto) {
    console.log(query);
    return this.booksService.getBooks(query);
  }

  @Version("1")
  @Post()
  createBook(@Body() body: BookDto) {
    return this.booksService.createBook(body);
  }

  @Version("1")
  @Put(":id")
  updateBook(@Param() param: UuidDto, @Body() body: UpdateBookDto) {
    return this.booksService.updateBook(param.id, body);
  }

  @Version("1")
  @Delete(":id")
  deleteBook(@Param() param: UuidDto) {
    return this.booksService.deleteBook(param.id);
  }
}
