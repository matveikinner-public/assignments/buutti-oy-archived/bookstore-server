import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export default class CreateBookDto {
  @ApiProperty({ type: String, description: "Book's Title", example: "Fantastic Mr. Fox" })
  @IsNotEmpty()
  @IsString()
  readonly title: string;

  @ApiProperty({ type: String, description: "Book's Author", example: "Roald Dahl" })
  @IsNotEmpty()
  @IsString()
  readonly author: string;

  @ApiProperty({ type: String, description: "Book's Description", example: "Lorem ipsum dolor sit amet" })
  @IsNotEmpty()
  @IsString()
  readonly description: string;
}
