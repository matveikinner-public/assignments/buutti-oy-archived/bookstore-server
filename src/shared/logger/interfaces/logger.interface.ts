import { LoggerService } from "@nestjs/common";

export interface ILoggerService extends LoggerService {
  fatal(message: string, ...args: any[]): void;
  setContext(context: string): void;
}
